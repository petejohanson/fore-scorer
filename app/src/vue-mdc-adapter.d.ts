
declare module 'vue-mdc-adapter' {
    import { PluginObject } from 'vue';

    const plugin: PluginObject<{}>;

    export = plugin;
}
