import { Actions, IState, Mutations } from './courses/types';

export const state: (() => IState) = () => ({
  courses: [{ name: 'NoHo State Grounds', url: 'http://localhost/api/courses/123' }],
});

export const mutations: Mutations = {
};

export const actions: Actions = {
};

export const namespaced = true;
