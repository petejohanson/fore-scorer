import { ICourse } from './courses/types';
import { IPlayer } from './players/types';

import { IUberDocument, parseResponse } from 'hubris';
import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { INewGameForm } from './games/types';

@Module({ namespaced: true, stateFactory: true })
export default class Games extends VuexModule {
  public newGameForm: INewGameForm | null = null;
  public currentGame: IUberDocument | null = null;

  @Mutation
  public CREATE_NEW_GAME_FORM() {
    this.newGameForm = { players: [] };
  }

  @Mutation
  public SELECT_NEW_GAME_COURSE(courseUrl: string) {
    if (!this.newGameForm) {
      this.newGameForm = { players: [] };
    }

    this.newGameForm.courseUrl = courseUrl;
  }

  @Mutation
  public ADD_NEW_GAME_PLAYER(player: IPlayer) {
    if (!this.newGameForm) {
      this.newGameForm = { players: [] };
    }

    this.newGameForm.players.push(player);
  }

  @Mutation
  public REMOVE_NEW_GAME_PLAYER(player: IPlayer) {
    if (!this.newGameForm) {
      this.newGameForm = { players: [] };
    }

    this.newGameForm.players = this.newGameForm.players.filter((p) => p.url !== player.url);
  }

  @Mutation
  public GAME_LOADED(game: IUberDocument) {
    this.currentGame = game;
  }

  @Action({ commit: 'CREATE_NEW_GAME_FORM' })
  public prepareNewGame() {
  }

  @Action({ commit: 'SELECT_NEW_GAME_COURSE' })
  public selectNewGameCourse({ url }: ICourse) {
    return url;
  }

  @Action({ commit: 'ADD_NEW_GAME_PLAYER' })
  public addNewGamePlayer(player: IPlayer) {
    return player;
  }

  @Action({ commit: 'REMOVE_NEW_GAME_PLAYER' })
  public removeNewGamePlayer(player: IPlayer) {
    return player;
  }

  @Action({ commit: 'GAME_LOADED' })
  public async loadGame(url: uri.URI) {
    return fetch(url.toString()).then(parseResponse);
  }
}
