import { GetterTree, MutationTree, ActionTree } from 'vuex';

export interface IPlayer {
  url: string;
  name: string;
  avatar?: string;
}

export interface IState {
  players: IPlayer[];
}

export type Getters = GetterTree<IState, any>;
export type Mutations = MutationTree<IState>;
export type Actions = ActionTree<IState, any>;
