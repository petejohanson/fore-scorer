import { GetterTree, MutationTree, ActionTree } from 'vuex';
import { IUberDocument } from 'hubris';
import { IPlayer } from '../players/types';

export interface INewGameForm {
  courseUrl?: string;
  players: IPlayer[];
}

export interface IState {
  newGameForm: INewGameForm | null;
  currentGame: IUberDocument | null;
}

export type Getters = GetterTree<IState, any>;
export type Mutations = MutationTree<IState>;
export type Actions = ActionTree<IState, any>;
