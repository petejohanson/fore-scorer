import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import games from './games';
import * as navigation from './navigation';
import * as courses from './courses';
import * as players from './players';

export default new Vuex.Store({
  state: {

  },
  mutations: {

  },
  actions: {

  },
  modules: {
    courses,
    games,
    navigation,
    players,
  },
});
