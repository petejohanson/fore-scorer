import { GetterTree, MutationTree, ActionTree } from 'vuex';

export interface ICourse {
  url: string;
  name: string;
  logo?: string;
}

export interface IState {
  courses: ICourse[];
}

export type Getters = GetterTree<IState, any>;
export type Mutations = MutationTree<IState>;
export type Actions = ActionTree<IState, any>;
