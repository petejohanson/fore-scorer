import { Actions, IState, Mutations } from './players/types';

export const state: (() => IState) = () => ({
  players: [{ name: 'John Wayne', url: 'https://localhost:8080/players/1234' }],
});

export const mutations: Mutations = {
};

export const actions: Actions = {
};

export const namespaced = true;
