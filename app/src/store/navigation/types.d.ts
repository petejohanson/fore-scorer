import { GetterTree, MutationTree, ActionTree } from 'vuex';

export type IMenu = 'menu';

export interface IIcon {
  icon: string;
}

export interface IContextMenuItem {
  icon?: string;
  label: string;
}

export interface IContextMenu {
  items: ReadonlyArray<IContextMenuItem>;
}

export type MainActionLocation = 'center' | 'end';

export type Item = IMenu | IIcon;

export interface IState {
  startNav: Item[];
  endNav: Item[];
  mainAction?: IIcon;
  mainActionLocation: MainActionLocation;
  mainActionSpeedDial: IIcon[];
  contextMenu?: IContextMenu;
}

export type Getters = GetterTree<IState, any>;
export type Mutations = MutationTree<IState>;
export type Actions = ActionTree<IState, any>;
