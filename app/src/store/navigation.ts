import {
  Actions,
  Getters,
  IContextMenu,
  IIcon,
  IMenu,
  IState,
  Item,
  MainActionLocation,
  Mutations,
} from './navigation/types';

export const state: (() => IState) = () => ({
  startNav: [],
  endNav: [],
  mainAction: undefined,
  mainActionLocation: 'center',
  mainActionSpeedDial: [],
  contextMenu: undefined,
});

export const mutations: Mutations = {
  setStartNav(s: IState, nav: Item[]) {
    s.startNav = nav;
  },
  setEndNav(s: IState, nav: Item[]) {
    s.endNav = nav;
  },
  setMainAction(s: IState, { action, location, speedDial }) {
    s.mainAction = action;
    s.mainActionLocation = location;
    s.mainActionSpeedDial = speedDial;
  },
  setContextMenu(s: IState, menu) {
    s.contextMenu = menu;
  },
};

export const actions: Actions = {
  setMainActionLocation({ commit, state: s }, mainActionLocation) {
    commit('setMainAction', { mainAction: s.mainAction, mainActionLocation });
  },

  setNavigation({ commit }, { start, end, mainAction, mainActionLocation, mainActionSpeedDial, contextMenu }) {
    commit('setStartNav', start);
    commit('setEndNav', end);
    commit('setContextMenu', contextMenu);
    commit('setMainAction', {
      action: mainAction,
      location: (mainActionLocation || 'center'),
      speedDial: mainActionSpeedDial,
    });
  },
};

export const namespaced = true;
