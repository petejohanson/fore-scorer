
type Maybe<T> = undefined | T;

export function map<T, U>(f: (t: T) => U) {
    return (t: Maybe<T>) => typeof(t) !== 'undefined' ? f(t) : undefined;
}

export function bind<T, U>(f: (t: T) => Maybe<U>) {
    return (t: Maybe<T>) => typeof(t) !== 'undefined' ? f(t) : undefined;
}
