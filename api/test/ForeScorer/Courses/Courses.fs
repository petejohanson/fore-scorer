module ForeScorer.Courses.Tests
  open FsCheck
  open FsCheck.Xunit

  open ForeScorer.Courses.Course

  type OverTwo =
    static member PositiveInt() =
      Arb.Default.PositiveInt()
      |> Arb.filter (fun (i) -> i.Get > 2)

  [<Property>]
  let ``should create an empty course of the requested size``(size:PositiveInt) =
    courseHoleCount (emptyCourse size.Get) = size.Get

  [<Property( Arbitrary=[| typeof<OverTwo> |] )>]
  let ``setParForHole should not affect other hole par values``(size:PositiveInt) =
    let setCheckArb =
      Gen.choose (0, size.Get - 1)
      |> Gen.map HoleNumber
      |> Gen.two
      |> Gen.where (fun (set, check) -> set <> check)
      |> Arb.fromGen

    Prop.forAll setCheckArb (fun (set, check) ->
      let initial = emptyCourse size.Get
      let updated = setParForHole (Par 5) set initial

      getHole check initial = getHole check updated
    )

  [<Property>]
  let ``setParForHole should update the specified value``(size:PositiveInt) =
    let holeNumber =
      Gen.choose (0, size.Get - 1)
      |> Gen.map HoleNumber
      |> Arb.fromGen

    Prop.forAll holeNumber (fun (set) ->
      let par     = Par 5
      let initial = emptyCourse size.Get
      let updated = setParForHole (Par 5) set initial

      Option.map getHolePar (getHole set updated) = Some par
    )
