function (doc) {
  emit([doc.identity.issuer, doc.identity.subject], doc._id);
};
