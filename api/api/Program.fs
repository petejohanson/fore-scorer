module ForeScorer.Program

open ForeScorer.KestrelInterop
open ForeScorer.Projections
open ForeScorer.CouchDB
open ForeScorer.Global

let setupCouchDB = async {
  let! db = createDatabase ForeScorer.Global.CouchDB.Client.Value ForeScorer.Global.CouchDB.Database.Value "users"
  
  match db with
  | Ok(_) ->
    return!
      { EmbeddedDesignDocument.Name = "users"; Document = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("api.couchdb.dist.users._design.users.json") }
        |> createOrUpdateDesignDocument ForeScorer.Global.CouchDB.Client.Value ForeScorer.Global.CouchDB.Database.Value "users"
  | Error(msg) ->
    printfn "Failed to create users database: %s" msg
    return Error(msg)
}

[<EntryPoint>]
let main _argv =
    // TODO: Move definitions of projections into respective modules/types.
    [ { UserProjection.Name = "course-read-model"; ResourceName = "api.ForeScorer.Courses.CourseReadModelProjection.js" } ]
      |> createOrUpdateProjections EventStore.ProjectionsManager.Value EventStore.Credentials.Value
      |> Async.RunSynchronously
      |> ignore

    match Async.RunSynchronously setupCouchDB with
    | Error (msg) -> failwith msg
    | Ok(status) -> printfn "Setup CouchDb: %O" status

    let configureApp =
        ApplicationBuilder.useArachnid Api.root

    WebHost.create ()
    |> WebHost.bindTo [|"http://0.0.0.0:5000"|]
    |> WebHost.configure configureApp
    |> WebHost.buildAndRun

    0
