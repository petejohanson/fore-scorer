module ForeScorer.Chiron

open Chiron

module JsonResult =

  let toOption =
    function
    | JPass p -> Some p
    | _ -> None