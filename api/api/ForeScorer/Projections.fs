module ForeScorer.Projections

open System.IO
open System.Reflection

open EventStore.ClientAPI.Projections
open EventStore.ClientAPI.SystemData

type UserProjection = {
  Name: string;
  ResourceName: string;
}

let createProjection (pm : ProjectionsManager) creds up = async {
  use stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(up.ResourceName)
  use reader = new StreamReader(stream)

  let! project = reader.ReadToEndAsync() |> Async.AwaitTask
  do! pm.CreateContinuousAsync (up.Name, project, creds) |> Async.AwaitTask
}

let createOrUpdateProjections (pm : ProjectionsManager) (creds: UserCredentials) (projections : UserProjection seq) = async {
  let! existing = pm.ListAllAsync(creds) |> Async.AwaitTask
  
  return!
    projections
    |> Seq.filter (fun p ->
        not (Seq.exists (fun (pd : ProjectionDetails) -> pd.Name = p.Name) existing))
    |> Seq.map (createProjection pm creds)
    |> Async.Parallel
    |> Async.Ignore
}