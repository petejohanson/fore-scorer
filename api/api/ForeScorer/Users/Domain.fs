module ForeScorer.Users.Domain

open ForeScorer.Aggregates

open Chiron
module E  = Chiron.Serialization.Json.Encode
module EI = Chiron.Inference.Json.Encode
module D  = Chiron.Serialization.Json.Decode
module DI = Chiron.Inference.Json.Decode

type Email = string

type PersonName =
  { FirstName : string
  ; LastName  : string
  } with
  static member ToJson (n : PersonName) : Json =
    JsonObject.empty
    |> EI.required "given" n.FirstName
    |> EI.required "family" n.LastName
    |> E.jsonObject
  static member FromJson (_ : PersonName) = jsonDecoder {
    let! given = DI.required "given"
    let! family = DI.required "family"

    return { FirstName = given; LastName = family }
  }

type OAuthIdentity =
  { Issuer  : string
  ; Subject : string
  } with
  static member ToJson (i : OAuthIdentity) : Json =
    JsonObject.empty
    |> EI.required "issuer" i.Issuer
    |> EI.required "subject" i.Subject
    |> E.jsonObject
  static member FromJson (_ : OAuthIdentity) = jsonDecoder {
    let! issuer = DI.required "issuer"
    let! subject = DI.required "subject"

    return { Issuer = issuer; Subject = subject }
  }  

type CreateCommand =
  | RegisterUser of OAuthIdentity * Email option

type CreateComment = unit

type State =
  { Metadata : AggregateMetadata option
  ; Identity : OAuthIdentity
  ; Email : Email option
  ; Name  : PersonName option
  } with
  static member ToJson (s : State) : Json =
    JsonObject.empty
    |> EI.optionalMixin s.Metadata
    |> EI.required "identity" s.Identity
    |> EI.optional "email" s.Email
    |> EI.optional "name" s.Name
    |> E.jsonObject
  static member FromJson (_ : State) = jsonDecoder {
    let! identity = DI.required "identity"
    let! email = DI.optional "email"
    let! name = DI.optional "name"
    let! metadata = E.jsonObject >> DI.requiredMixin

    return { Metadata = Some(metadata)
           ; Identity = identity
           ; Email = email
           ; Name = name
           }
  }  


let handleCreate =
  function
  | RegisterUser (identity, email) -> { Metadata = None; Identity = identity; Email = email; Name = None }

let handle (state: State) (command : CreateCommand) = state

let aggregate =
  { handleCreate = handleCreate
  ; handleUpdate = handle
  }