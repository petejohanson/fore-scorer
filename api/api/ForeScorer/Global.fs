namespace ForeScorer.Global

open EventStore.ClientAPI.Common.Log
open EventStore.ClientAPI.Projections
open EventStore.ClientAPI.SystemData
open System
open System.Net
open System.Net.Http
open System.Net.Sockets
open ForeScorer.EventStore
open System.IdentityModel.Tokens.Jwt
open Microsoft.IdentityModel.Tokens
open Chiron.Serialization.Json.Encode

type CouchDB() =
  static member Client : Lazy<HttpClient> = lazy(
    new HttpClient()
  )
  static member Database : Lazy<Uri> = lazy(
    "COUCHDB_DATABASE"
      |> Environment.GetEnvironmentVariable
      |> Uri
  )

type EventStore() =
  static member Connection = lazy (
    "TREE_RINGS_EVENT_STORE"
      |> Environment.GetEnvironmentVariable
      |> connect
  )
  static member ApiUri = lazy (
    "TREE_RINGS_EVENT_STORE_REST_API"
      |> Environment.GetEnvironmentVariable
      |> Uri
  )
  static member Credentials = lazy (
    match EventStore.ApiUri.Value.UserInfo.Split([|':'|], 2) with
      | [| user; password |] -> UserCredentials(user, password)
      | _ -> null
  )
  static member ProjectionsManager = lazy (
    let uri = EventStore.ApiUri.Value
    let endpoints = Dns.GetHostAddresses(uri.DnsSafeHost)

    let ep = Seq.find (fun (e: IPAddress) -> e.AddressFamily = AddressFamily.InterNetwork) endpoints

    ProjectionsManager(ConsoleLogger(), IPEndPoint(ep, uri.Port), TimeSpan.FromMinutes(1.0))
  )

type OAuth() =
  static member private GetCredentials envVariable =
    let bytes =
      try
        envVariable
        |> Environment.GetEnvironmentVariable
        |> Convert.FromBase64String
      with
      | x -> raise x

    SigningCredentials(
      SymmetricSecurityKey(bytes),
      SecurityAlgorithms.HmacSha256Signature
    )
  static member JwtTokenHandler = lazy (
    JwtSecurityTokenHandler()
  )

  static member CodeSigningCredentials = lazy (
    OAuth.GetCredentials "TREE_RINGS_OAUTH_CODE_KEY"
  )

  static member TokenSigningCredentials = lazy (
    OAuth.GetCredentials "TREE_RINGS_OAUTH_TOKEN_KEY"
  )
