module ForeScorer.Courses.Course

open Chiron
module E = Chiron.Serialization.Json.Encode
module D = Chiron.Serialization.Json.Decode
module DI = Chiron.Inference.Json.Decode
module EI = Chiron.Inference.Json.Encode
open Chiron.Operators
open ForeScorer.EventSource

type Par = Par of int
type HoleNumber = HoleNumber of int
type Hole = Hole of HoleNumber * Par with
  static member ToJson (Hole (HoleNumber i, Par p)): Json =
    JsonObject.empty
    |> EI.required "number" i
    |> EI.required "par" p
    |> E.jsonObject

type Course = Course of Hole list with
  static member ToJson (Course holes) : Json =
    JsonObject.empty
    |> EI.required "holes" holes
    |> E.jsonObject

let getHoles (Course holes) = holes

let getHoleNumber = function (Hole (n, p)) -> n

let getHolePar = function (Hole (n, p)) -> p

let emptyCourse size = Course <| List.init size (fun i -> Hole (HoleNumber i, Par 3))

let courseHoleCount (Course c) = List.length c

let setParForHole p h c =
  getHoles c
  |> List.map (function | (Hole (_h, _)) when h=_h -> (Hole (h, p)) | x -> x)
  |> Course

let getHole (h : HoleNumber) (Course c) = List.tryFind (getHoleNumber >> ((=) h)) c

type Command =
  | Create of name: string * size: int
  | SetParForHole of HoleNumber * Par

type Event =
  | Created of name: string * size: int
  | ParSetForHole of hole: int * par: int with
  static member ToJson (e : Event) : Json =
    match e with
    | Created (name, size) ->
        JsonObject.empty |> EI.required "Created" (E.propertyList [ ("name", E.string name); ("size", E.int size) ]) |> E.jsonObject
    | ParSetForHole (hole, par) ->
        JsonObject.empty |> EI.required "PreSetForHole" (hole, par) |> E.jsonObject
  static member private nameSizeFromJson : Decoder<JsonObject, string * int> =
    (fun n s -> n, s)
    <!> DI.required "name"
    <*> DI.required "size"
  static member private holeParFromJson : Decoder<JsonObject, int * int> =
    (fun h p -> h, p)
    <!> DI.required "hole"
    <*> DI.required "par"
  static member FromJson (_ : Event) : Decoder<Json,Event> =
    function
    | PropertyWith (D.jsonObjectWith Event.nameSizeFromJson) "Created" (name, size) as json ->
      JsonResult.pass(Created (name, size))
    | PropertyWith (D.jsonObjectWith Event.holeParFromJson) "ParSetForHole" (hole, par) as json ->
      JsonResult.pass(ParSetForHole (hole, par))
    | _ -> failwith "Unacceptable JSON to parse"    

      

let handle (course : Course) =
  function
  | Create (name, size) -> Created (name, size) |> publish
  | SetParForHole (HoleNumber hole, Par par) -> ParSetForHole (hole, par) |> publish

let apply course =
  function
    | Created (name, size)       -> emptyCourse size
    | ParSetForHole (hole, par) -> setParForHole (Par par) (HoleNumber hole) course

let aggregate =
  { init   = emptyCourse 0
  ; handle = handle
  ; apply  = apply
  }
