fromCategory('course')
  .foreachStream()
  .when({
    $init: () => ({ name: null, size: 0, holes: [] }),
    Created (state, evnt) {
      let { name, size } = evnt.body;
      state.name = name;
      state.size = size;
      state.holes = new Array(size).fill({ par: 3, description: null });
    },
    ParSetForHole (state, evnt) {
      let { hole, par } = evnt.body;

      state.holes[hole].par = par;
    }
  })
  .outputState();