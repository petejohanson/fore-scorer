module ForeScorer.Courses.Api

open ForeScorer.EventStore
open ForeScorer.EventSource
open ForeScorer.Global
open ForeScorer.Courses.Course
open ForeScorer.Arachnid

open System
open System.IO
open System.Text

open Aether
open Chiron
module D  = Chiron.Serialization.Json.Decode
module E  = Chiron.Serialization.Json.Encode
module EI = Chiron.Inference.Json.Encode
module DI = Chiron.Inference.Json.Decode
open Chiron.Operators
open Aether.Operators
open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Core.Optics
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Routers
open Arachnid.Routers.Uri.Template
open Arachnid.Types.Http
open Arachnid.Types.Uri.Template

type CourseHoleReadModel = {
  Par: int;
  Description: string option;
} with
  static member FromJson (_: CourseHoleReadModel) = jsonDecoder {
    let! p = DI.required "par"
    let! d = D.required (D.optionWith D.string) "description"

    return { Par = p; Description = d }
  }

  static member ToJson(h : CourseHoleReadModel) : Json =
    JsonObject.empty
    |> EI.required "par" h.Par
    |> EI.optional "description" h.Description
    |> E.jsonObject

type CourseReadModel = {
  Name: string;
  Size: int;
  Holes: CourseHoleReadModel list;
} with
  static member FromJson (_ : CourseReadModel) = jsonDecoder {
    let! name = DI.required "name"
    let! size = DI.required "size"
    let! holes = DI.required "holes"

    return { Name = name; Size = size; Holes = holes }
  }

  static member ToJson (rm : CourseReadModel) : Json =
    JsonObject.empty
    |> EI.required "name" rm.Name
    |> EI.required "size" rm.Size
    |> EI.required "holes" rm.Holes
    |> E.jsonObject

type CreateCourseInputModel = {
  Name: string;
  Size: int;
} with
  static member FromJson (_: CreateCourseInputModel) = jsonDecoder {
    let! name = DI.required "name"
    let! size = DI.required "size"

    return
      { Name = name
      ; Size = size
      }
  }

let private handler = async {
  let! es = EventStore.Connection.Value
  let repo = createRepository es "course"
  return createHandler Course.aggregate repo
}

let private courseGetter : System.Guid -> Async<CourseReadModel option> =
  let pm = EventStore.ProjectionsManager.Value
  createReadModelGetter pm "course" "course-read-model"
let private courseCollection =
    arachnid {
        return Course.emptyCourse 18
    }

let private empty x =  Representation.empty

let private createdLocation (template : Arachnid.Types.Uri.Template.UriTemplate) (id : System.Guid) = arachnid {
  let location =
    [ (Key "id", Atom (id.ToString()))]
    |> Map
    |> UriTemplateData
    |> template.Render
    |> Location.parse
    |> Some

  do! Arachnid.Optic.set Response.Headers.location_ location

  return ()
}

let private representCreated =
  function
  | Choice1Of2 id -> (fun a -> Representation.empty) <!> createdLocation (UriTemplate.parse "/courses{/id}") id
  | Choice2Of2 ex -> Arachnid.init Representation.empty

let private createCourse (input : CreateCourseInputModel) = async {
    let! h = handler
    let id = System.Guid.NewGuid()
    let command = Course.Create (input.Name, input.Size)
    let! result = h id command

    // Exception handling?!
    match result with
      | None -> return Choice1Of2 id
      | Some ex -> return Choice2Of2 ex
  }

// TODO: Handle bogus payload w/ missing data, etc! Needs to Option.map/bind instead!
let private postCourse = Arachnid.memo(Helpers.payload () >>= (Option.get >> createCourse >> Arachnid.fromAsync))

let courseCollectionMachine =
  arachnidMachine {
    methods [ GET; OPTIONS; POST ]
    created true
    doPost(ignore <!> postCourse)
    handleOk(Helpers.represent <!> courseCollection)
    handleCreated(postCourse >>= representCreated)
  }

let private guid_ : Epimorphism<string, Guid> =
  (fun x ->
            match Guid.TryParse x with
            | true, x -> Some x
            | _ -> None),
  (string)

let private courseId_ = Arachnid.memo(Arachnid.Optic.get (Route.atom_ "id" >?> guid_))

let course_ = Arachnid.memo(courseId_ >>= (fun mi ->
  match mi with
  | None -> Arachnid.init(None)
  | Some id -> courseGetter id |> Arachnid.fromAsync
))

let courseMachine =
  arachnidMachine {
    methods [ GET; OPTIONS; HEAD; ]
    exists(Option.isSome <!> course_)
    handleOk(Helpers.represent <!> course_)
  }