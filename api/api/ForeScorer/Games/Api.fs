module ForeScorer.Games.Api

open ForeScorer.Arachnid

open Aether
open Chiron
module D  = Chiron.Serialization.Json.Decode
module E  = Chiron.Serialization.Json.Encode
module EI = Chiron.Inference.Json.Encode
module DI = Chiron.Inference.Json.Decode
open Chiron.Operators
open Aether.Operators
open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Core.Optics
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Routers
open Arachnid.Routers.Uri.Template
open Arachnid.Types.Http
open Arachnid.Types.Uri
open Arachnid.Types.Uri.Template
open Arachnid.Uber

let private gameForm = [
  { Data.empty with
      Id = Some("course");
      Label = Some("Select Course");
      Data =
        [ { Data.empty with Rel = ["search"]; Url = Some(Url.UriTemplate(UriTemplate.parse "courses/{?q*}")) }
        ; { Data.empty with Rel = ["favorites"]; Url = Some(Url(Arachnid.Types.Uri.UriReference.parse "courses/favorites")) }
        ];
  }
  { Data.empty with
      Id = Some("players");
      Label = Some("Select Players");
      Data =
        [ { Data.empty with Rel = ["search"]; Url = Some(Url.UriTemplate(UriTemplate.parse "players/{?q*}")) }
        ; { Data.empty with Rel = ["favorites"]; Url = Some(Url(Arachnid.Types.Uri.UriReference.parse "players/favorites")) }
        ];
  }
]

let gameFormLink =
  { Data.empty with
      Url = Some(Url(Arachnid.Types.Uri.UriReference.parse "games/"));
      Rel = ["create-form"];
      Id = Some("create-game-form");
      Label = Some("New Game");
      Data = gameForm;
  }

let private gameData = [
  { Data.empty with Url = Some(Url(Arachnid.Types.Uri.UriReference.parse "games/")); Rel = ["self"] }
]

let private gameFormDocument = Arachnid.Uber.Document (Some(Version1_0), gameData, None)

let gameCollectionMachine =
  arachnidMachine {
    methods [ GET; OPTIONS; HEAD; ]
    exists true
    handleOk(Helpers.representUber gameFormDocument)
  }

let scorecards =
  [ { Data.empty with
        Url = ("/games/123/scores/1" |> UriReference.parse |> Url.Url |> Some);
        Data =
          [
            Data.basicLink ["player"] ("/players/1234" |> UriReference.parse |> Url.Url)
            { Data.empty with
                Rel = ["collection"];
                Data =
                  [
                    { Data.empty with
                        Data =
                          [
                            Data.formField "value" "Value" (Value.Int 2)
                            Data.formField "hole" "Hole #" (Value.Int 1)
                            Data.formField "timestamp" "Recorded At" (Value.Null)
                          ]
                    }
                  ]
            }
          ]
    }

  ]
let gameDocument =
  [ Data.basicLink ["self"] ("/games/123" |> UriReference.parse |> Url.Url )
  ; { Data.basicLink ["course"] ("/courses/123" |> UriReference.parse |> Url.Url) with
        Data =
          [ Data.formField "name" "Name" (Value.String "NoHo State Hospital")

          ];
    }
  ; { Data.empty with
        Rel = ["scorecards"];
        Data = scorecards
    }
  ] |> Document.data

let gameMachine =
  arachnidMachine {
    methods [ GET; OPTIONS; HEAD; ]
    exists true
    handleOk(Helpers.representUber gameDocument)
  }