module ForeScorer.EventSource

open System

type Aggregate<'TState, 'TCommand, 'TEvent> = {
  init: 'TState;
  handle: 'TState -> 'TCommand -> Choice<'TEvent, Exception>;
  apply: 'TState -> 'TEvent -> 'TState;
}

let publish = Choice1Of2
let fail = Choice2Of2

type Id = System.Guid

let inline createHandler<'TState, 'TCommand, 'TEvent> (aggregate: Aggregate<'TState, 'TCommand, 'TEvent>) (loader: Id -> Async<'TEvent seq>, commit: Id -> 'TEvent -> Async<unit>) =
  fun id command -> async {
    let! events = loader id
    let state = Seq.fold aggregate.apply aggregate.init events
    let event = aggregate.handle state command

    match event with
    | Choice1Of2 event ->
      do! commit id event
      return None
    | Choice2Of2 exn ->
      return exn |> Some
  }
