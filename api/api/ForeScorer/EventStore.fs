module ForeScorer.EventStore

open System
open System.Text
open Chiron
module E  = Chiron.Serialization.Json.Encode
module JI = Chiron.Inference.Json
module DI = Chiron.Inference.Json.Decode
open EventStore.ClientAPI
open EventStore.ClientAPI.Projections
open Microsoft.FSharp.Reflection

let connect (str: string) = async {
  let conn = str |> Uri |> EventStoreConnection.Create
  do! conn.ConnectAsync() |> Async.AwaitTask

  return conn
}

let streamName category (id: Guid) = category + "-" + id.ToString("N").ToLower()


let inline createRepository (conn: IEventStoreConnection) category =
  let inline loader id : Async<'TResult seq> = async {
    let stream = streamName category id
    let! events = conn.ReadStreamEventsForwardAsync(stream, 1L, 4096, false) |> Async.AwaitTask

    let parseEvent (e : RecordedEvent) : 'TResult =
      e.Data
      |> Encoding.UTF8.GetString
      |> Json.parse
      |> JsonResult.map(fun j -> JsonObject.ofPropertyList [(e.EventType, j)] |> E.jsonObject)
      |> JsonResult.bind JI.decode
      |> JsonResult.getOrThrow

    return events.Events |> Seq.map (fun ev -> parseEvent ev.Event)
  }

  let inline commit (id: Guid) event = async {
    let stream = streamName category id
    let case = FSharpValue.GetUnionFields (event, null) |> fst    
    let eventType = case.Name
    
    let json = event |> JI.encode
    let subset = json |> Optics.get (Optics.Json.Property_ case.Name)
    let content = match subset with
                  | JPass a -> a
                  | JFail _ -> json

    let data = content |> Json.format |> Encoding.UTF8.GetBytes
    
    let metadata = [||] : byte array
    let eventData = EventData(Guid.NewGuid(), eventType, true, data, metadata)

    // TODO: Proper event versions!
    return! conn.AppendToStreamAsync(stream, ExpectedVersion.Any, eventData)
      |> Async.AwaitIAsyncResult
      |> Async.Ignore
  }

  loader,commit

let inline createReadModelGetter (pm: ProjectionsManager) category projection =
  let inline getter (id: Guid) = async {
    let partition = streamName category id
    let! result =
      pm.GetPartitionResultAsync(projection, partition)
      |> Async.AwaitTask

    return match result with
             | null -> None
             | "" -> None
             | s -> s |> JI.deserialize |> JsonResult.getOrThrow |> Some
  }

  getter