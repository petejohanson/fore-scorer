module ForeScorer.CouchDB

open System
open System.IO
open System.Net.Http
open System.Text

open Arachnid.Types.Uri.Template

open Chiron
module JI = Chiron.Inference.Json
module D  = Chiron.Serialization.Json.Decode
module DI = Chiron.Inference.Json.Decode

open ForeScorer.Chiron

type EmbeddedDesignDocument =
  { Name : string
  ; Document: Stream
  }

let private responseToResult (resp : HttpResponseMessage) =
  if resp.IsSuccessStatusCode then
    Ok (resp.StatusCode)
  else
    Error resp.ReasonPhrase  

let createDatabase (client : HttpClient) (db : Uri) (name : string) = async {
  let databaseUri = Uri(db, name)

  use headReq = new HttpRequestMessage(HttpMethod.Head, databaseUri)
  let! headResp = client.SendAsync(headReq) |> Async.AwaitTask

  match responseToResult headResp with
  | Ok (status) -> return Ok(status)
  | Error (_) ->
    let! putResp = client.PutAsync(databaseUri, null) |> Async.AwaitTask
    return responseToResult putResp
}

let createOrUpdateDesignDocument (client : HttpClient) (db : Uri) (name : string) ({ Name = docName; Document = doc }) = async {
  let baseUri = Uri(db, name + "/")
  let designDoc = Uri(baseUri, sprintf "_design/%s" docName)

  use headReq = new HttpRequestMessage(HttpMethod.Head, designDoc)
  let! headResp = client.SendAsync(headReq) |> Async.AwaitTask

  if headResp.IsSuccessStatusCode then
    // TODO: Allow updating existing design documents! JSON object merge?
    return Ok ()
  else  

    use content = new StreamContent(doc)
    let! resp = client.PutAsync(designDoc, content) |> Async.AwaitTask

    if resp.IsSuccessStatusCode then
      return Ok ()
    else
      return Error(resp.ReasonPhrase)  
}

let inline makeRepository (client : HttpClient) (db: Uri) (name : string) =
  let baseUri = Uri(db, name + "/")
  let set (id : String) agg = async {
    let u = Uri(baseUri, id)
    let! _res = client.PutAsync(u, agg |> JI.serialize |> fun s -> new StringContent(s, Encoding.UTF8, "application/json")) |> Async.AwaitTask

    return ()
  }

  let get (id : String) = async {
    let u = Uri(baseUri, id)
    let! s = client.GetStringAsync(u) |> Async.AwaitTask

    return s |> JI.deserialize |> JsonResult.toOption
  }  

  (get, set)

type QueryKey<'T> =
  | SingleKey of 'T
  | RangeKey of startKey : 'T * endKey : 'T

let private queryKeyWith (decoder : Decoder<Json, 'a>) : Decoder<Json, QueryKey<'a>> =
  Decoder.map SingleKey decoder

type QueryResponseRow<'TKey, 'TValue> =
  { Id : string
  ; Key : QueryKey<'TKey>
  ; Value : 'TValue
  }

let queryResponseRowWith (keyDecoder : Decoder<Json, 'key>) (valueDecoder : Decoder<Json, 'value>) : Decoder<Json, QueryResponseRow<'key,'value>> = jsonDecoder {
  let! id = DI.required "id"
  let! key = D.required (queryKeyWith keyDecoder) "key"
  let! value = D.required valueDecoder "value"

  return { Id = id; Key = key; Value = value }
}

type QueryResponse<'TKey, 'TValue> =
  { RowCount : int
  ; Offset : int
  ; Rows : QueryResponseRow<'TKey, 'TValue> list
  }

let queryResponseWith (keyDecoder : Decoder<Json, 'key>) (valueDecoder : Decoder<Json, 'value>) : Decoder<Json, QueryResponse<'key,'value>> = jsonDecoder {
  let! count = DI.required "total_rows"
  let! offset = DI.required "offset"
  let! rows = D.required (D.listWith (queryResponseRowWith keyDecoder valueDecoder)) "rows"

  return { RowCount = count; Offset = offset; Rows = rows }
}
let private viewTemplate = UriTemplate.parse "_design/{name}/_view/{view_name}{?key,startkey,endkey}"

let getQueryContent (client : HttpClient) (dbUri : Uri) (data : UriTemplateData) =
  let rendered = Rendering.render UriTemplate.Rendering.Render data viewTemplate

  let finalUri = Uri(dbUri, rendered)
  // TODO: Handling of error codes!
  client.GetStringAsync(finalUri) |> Async.AwaitTask

let inline makeQuery (client : HttpClient) (server : Uri) (db : string) (viewName: string) : QueryKey<'TKey> -> Async<Option<QueryResponseRow<'TKey, 'TValue>>> =
  let dbUri = Uri(server, db + "/")
  let baselineTemplateData =
    UriTemplateData(
      Map
        [ Key "name", Atom db
        ; Key "view_name", Atom viewName
        ]
    )

  let findOne (key : QueryKey<'TKey>) = async {
    let keyParams = match key with
                    | SingleKey (key) ->
                      Map
                        [ Key "key", key |> JI.serialize |> Atom ]
                    | RangeKey (startKey, endKey) ->
                      Map
                        [ Key "startkey", startKey |> JI.serialize |> Atom
                        ; Key "endkey", endKey |> JI.serialize |> Atom
                        ]

    let templateData = baselineTemplateData + UriTemplateData(keyParams)
    
    let! s = getQueryContent client dbUri templateData
    
    return
      s
      |> Json.deserializeWith (queryResponseWith JI.decode JI.decode)
      |> JsonResult.map (fun r -> List.tryHead r.Rows)
      |> JsonResult.toOption
      |> Option.flatten
  }

  findOne