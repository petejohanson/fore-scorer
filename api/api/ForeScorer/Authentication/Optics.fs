module ForeScorer.Authentication.Optics

open Aether
open Aether.Operators
open Arachnid.Core
open System.IdentityModel.Tokens.Jwt
open System.Security.Claims
open System.IdentityModel.Tokens.Jwt
open System.IdentityModel.Tokens.Jwt

type JwtSecurityToken with
  static member item_ (check : 'i -> bool) : Prism<seq<'i>, 'i> =
    let get = Seq.tryFind check
    let set = fun (item : 'i) ->
      Seq.map (fun c -> match check c with | true -> item | false -> c)

    (get,set)

  static member claims_ : Lens<JwtSecurityToken, Claim seq> =
    let get = fun (t : JwtSecurityToken) -> t.Claims
    let set = fun (c : Claim seq) (t : JwtSecurityToken) ->
      JwtSecurityToken(
        t.Issuer,
        Seq.head t.Audiences,
        c,
        System.Nullable(t.ValidFrom),
        System.Nullable(t.ValidTo),
        t.SigningCredentials
      ) 
    
    (get, set)

  static member claim_ key =
        JwtSecurityToken.claims_
    >-> JwtSecurityToken.item_ (fun (c : Claim) -> c.Type = key)

  static member email_ = JwtSecurityToken.claim_ "email"


module Request =
  let private key = "treerings.authentication.bearer_token"
  
  let bearerToken_ =
    State.value_<JwtSecurityToken> key

  let private key_ =
    State.key_<JwtSecurityToken> key

  let claims_ =
        key_
    >?> JwtSecurityToken.claims_

  let subject_ =
        key_
    >?> JwtSecurityToken.claim_ "sub"    