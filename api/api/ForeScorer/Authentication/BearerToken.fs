module ForeScorer.Authentication.BearerToken

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Optics.Http
open Microsoft.IdentityModel.Tokens
open System.IdentityModel.Tokens.Jwt
open System.Text.RegularExpressions
open ForeScorer.Global
open ForeScorer.Authentication.Optics

let private authRegex = Regex("^(?<type>\S+) (?<value>.*)$", RegexOptions.Compiled)
let private (|AuthorizationType|_|) authType input =
  let m = authRegex.Match(input)

  if m.Success then
    match m.Groups.["type"].Value, m.Groups.["value"].Value with
    | t, value when t = authType -> Some(value)
    | _,_ -> None
  else None  

let private validateToken parameters token =
  try
    let (_cp, token) = OAuth.JwtTokenHandler.Value.ValidateToken(token, parameters)

    token :?> JwtSecurityToken |> Some
  with
    | :? SecurityTokenException -> None  

let tokenParser = arachnid {
  let! header = !. (Request.header_ "Authorization")

  match header with
  | Some(AuthorizationType "Bearer" tokenStr) ->
    // TODO: More complete validation!
    let validationParams = TokenValidationParameters(
                               ValidIssuer = "http://localhost:5000",
                               ValidAudience = "http://localhost:5000",
                               IssuerSigningKey = ForeScorer.Global.OAuth.TokenSigningCredentials.Value.Key
                             )
    let token = validateToken validationParams tokenStr
    do! Request.bearerToken_ .= token

    return token
  | _ -> return None
}