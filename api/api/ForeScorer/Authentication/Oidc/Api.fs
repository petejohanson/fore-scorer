module ForeScorer.Authentication.Oidc.Api

open System

open Chiron
module D  = Chiron.Serialization.Json.Decode
module E  = Chiron.Serialization.Json.Encode
module JI = Chiron.Inference.Json
module DI = Chiron.Inference.Json.Decode

open Aether.Operators
open Arachnid.Core
open Arachnid.Routers.Uri.Template
open Arachnid.Machines.Http
open Arachnid.Types.Http
open Arachnid.Types.Http.State
open Arachnid.Types.Uri
open Arachnid.Optics.Http

open ForeScorer.Arachnid.Optics.Cookies
open ForeScorer.Authentication.Oidc.Domain
open ForeScorer.Chiron

open Microsoft.IdentityModel.Tokens
open System.IdentityModel.Tokens.Jwt
open System.Security.Claims
open System.Security.Cryptography


module Types =
  open Chiron.Operators
  open System.Text

  type Configuration =
    { Issuer : Uri
    ; AuthorizationEndpoint : Uri
    ; TokenEndpoint : Uri
    ; ScopesSupported : Scope list
    } with
    static member ToJson(c : Configuration) =
      JsonObject.empty
      |> E.required E.string "issuer" (Uri.format c.Issuer)
      |> E.required E.string "authorization_endpoint" (Uri.format c.AuthorizationEndpoint)
      |> E.required E.string "token_endpoint" (Uri.format c.TokenEndpoint)
      |> E.jsonObject

  type ErrorCode =
    | InvalidRequest
    | InvalidClient
    | UnauthorizedClient
    | AccessDenied
    | UnsupportedResponseType
    | InvalidScope
    | InvalidGrant
    | UnsupportedGrantType
    | ServerError
    | TemporarilyUnavailable
    | Custom of custom : string
    with
      static member parse =
        function
          | "invalid_request" -> InvalidRequest
          | "invalid_client" -> InvalidClient
          | "unauthorized_client" -> UnauthorizedClient
          | "access_denied" -> AccessDenied
          | "unsupported_response_type" -> UnsupportedResponseType
          | "invalid_scope" -> InvalidScope
          | "invalid_grant" -> InvalidGrant
          | "unsupported_grant_type" -> UnsupportedGrantType
          | "server_error" -> ServerError
          | "temporarily_unavailable" -> TemporarilyUnavailable
          | custom -> Custom custom

      static member format =
        function
          | InvalidRequest -> "invalid_request"
          | InvalidClient -> "invalid_client"
          | UnauthorizedClient -> "unauthorized_client"
          | AccessDenied -> "access_denied"
          | UnsupportedResponseType -> "unsupported_response_type"
          | InvalidScope -> "invalid_scope"
          | InvalidGrant -> "invalid_grant"
          | UnsupportedGrantType -> "unsupported_grant_type"
          | ServerError -> "server_error"
          | TemporarilyUnavailable -> "temporarily_unavailable"
          | Custom (c) -> c

  let uriDecoder =
    D.string >=> Decoder.fromThrowingConverter Uri.parse

  type StateWrapper =
    { InnerState : string option
    ; RedirectUri : Arachnid.Types.Uri.Uri
    ; Nonce : string option
    } with
    static member empty = { InnerState = None; RedirectUri = Uri.parse "about:blank"; Nonce = None }

    static member FromJson(_ : StateWrapper) = jsonDecoder {
      let! inner = D.optional D.string "s"
      let! redirect = D.required D.string "r"
      let! nonce = D.optional D.string "n"

      return { InnerState = inner
             ; RedirectUri = Uri.parse redirect
             ; Nonce = nonce
             }
    }
    static member ToJson (sw : StateWrapper) =
      JsonObject.empty
      |> E.optional E.string "s" sw.InnerState
      |> E.required E.string "r" (string sw.RedirectUri)
      |> E.optional E.string "n" sw.Nonce
      |> E.jsonObject

  type AuthorizationError =
    { Error : ErrorCode
    ; Description : string option
    ; Uri : Uri option
    ; State : StateWrapper
    } with
    static member ToJson (e : AuthorizationError) =
      JsonObject.empty
      |> E.required E.string "error" (ErrorCode.format e.Error)
      |> E.optional E.string "error_description" e.Description
      |> E.optional E.string "error_uri" (Option.map Uri.format e.Uri)
      |> E.jsonObject

  type AuthorizationSuccess =
    { Code : string
    ; State : StateWrapper
    }

  type AuthorizationResponse = Result<AuthorizationSuccess, AuthorizationError>

  let getResponseRedirectUri =
    function
    | Ok({ AuthorizationSuccess.State = state }) -> state.RedirectUri
    | Error({ AuthorizationError.State = state }) -> state.RedirectUri
  let authorizationResponse_ : Aether.Epimorphism<Query, AuthorizationResponse> =
    (fun q ->
      (fst Query.pairs_) q
      |> Option.map (
        fun pairs ->
          let tryFindValue =
            fun k1 ->
                 List.tryFind(fun (k2,_) -> k1 = k2) pairs
              |> (Option.map snd)
              |> Option.flatten

          let innerState =
            "state"
            |> tryFindValue
            |> Option.map (
                  Convert.FromBase64String
               >> Encoding.UTF8.GetString
               >> JI.deserialize
               >> JsonResult.toOption
            )
            |> Option.flatten
            |> Option.defaultValue StateWrapper.empty

          match tryFindValue "error", tryFindValue "code" with
          | Some(error), _ ->
            let desc = tryFindValue "error_description"
            let uri = tryFindValue "error_uri" |> Option.map Uri.parse

            Error { Error = ErrorCode.parse error; Description = desc; Uri = uri; State = innerState }
          | _, Some(code) ->
            Ok { Code = code; AuthorizationSuccess.State = innerState }
          | _,_ -> Error { Error = Custom("invalid_response"); Description = Some("Authorization response without code or error details") ; Uri = None; State = innerState }
      )
    ),
    (
        function
        | Ok(success) ->
          [ "code", Some(success.Code)
          ; "state", success.State.InnerState
          ]
        | Error(err) ->
          [ "error", err.Error |> ErrorCode.format |> Some
          ; "error_description", err.Description
          ; "error_uri", Option.map Uri.format err.Uri
          ; "state", Some(err.State |> JI.serialize |> System.Text.Encoding.UTF8.GetBytes |> Convert.ToBase64String)
          ]
      >> List.filter (snd >> Option.isSome)
      >> (snd Query.pairs_)
    )

  type TokenSuccess =
    { AccessToken : JwtSecurityToken
    ; TokenType : string
    ; RefreshToken : JwtSecurityToken option
    ; ExpiresIn : TimeSpan
    ; Scope : Scope list option
    } with
    static member ToJson (s : TokenSuccess) =
      let wt = ForeScorer.Global.OAuth.JwtTokenHandler.Value.WriteToken
      let at = wt s.AccessToken

      JsonObject.empty
      |> E.required E.string "access_token" at
      |> E.required E.string "token_type" s.TokenType
      |> E.required E.int "expires_in" (int s.ExpiresIn.TotalSeconds)
      |> E.optional E.string "refresh_token" (Option.map wt s.RefreshToken)
      |> E.optional E.stringList "scope" (Option.map (List.map string) s.Scope)
      |> E.jsonObject

module Resources =
  open Arachnid.Core.Operators
  open ForeScorer.Aggregates
  open ForeScorer.CouchDB
  open ForeScorer.Arachnid
  open ForeScorer.Arachnid.Optics.Http
  open ForeScorer.Authentication.Optics

  let path_ = Uri.hierarchyPart_ >-> HierarchyPart.authority_ >?> Aether.Optics.snd_ >?> PathAbsoluteOrEmpty.raw_

  let userHandler, userCreateHandler =
    createHandler
      (makeRepository ForeScorer.Global.CouchDB.Client.Value ForeScorer.Global.CouchDB.Database.Value "users")
      ForeScorer.Users.Domain.aggregate

  let cryptoProvider = new RNGCryptoServiceProvider()

  let generateNonce () =
    let bytes = Array.zeroCreate 128
    cryptoProvider.GetBytes(bytes)

    Convert.ToBase64String bytes

  let userQuery : QueryKey<string list> -> Async<Option<QueryResponseRow<string list, string>>> = makeQuery ForeScorer.Global.CouchDB.Client.Value ForeScorer.Global.CouchDB.Database.Value "users" "identity"  

  let nonceCookieName = "fore-scorer-oauth-nonce"

  let private badRedirect =
    function
    | None -> Arachnid.init(true)
    | Some(Error(_)) -> Arachnid.init(true)
    | Some(Ok ({ Types.Code = _code; Types.AuthorizationSuccess.State = state})) -> arachnid {
      let! nonce = !. (Request.cookie_ (Name.Name nonceCookieName) >-> Option.mapLens (Pair.value_ >-> Value.value_))

      return nonce <> state.Nonce
    }

  let redirectResponse : Arachnid<Types.AuthorizationResponse option> = Arachnid.memo !. (Request.query_ >-> Types.authorizationResponse_)

  let processRedirect (config : ProviderConfig) (resp : Types.AuthorizationResponse) = arachnid {
    match resp with
    | Ok { Types.Code = code; Types.AuthorizationSuccess.State = state } ->
      let! t = getToken config { Code = code; RedirectUri = Uri.parse "http://localhost:5000/auth/google/redirect" } |> Arachnid.fromAsync

      match t with
      | Some (token) ->
        let identity = { ForeScorer.Users.Domain.OAuthIdentity.Issuer = token.IdToken.Issuer; ForeScorer.Users.Domain.OAuthIdentity.Subject = token.IdToken.Subject }

        let! findExisting = userQuery (SingleKey [ identity.Issuer; identity.Subject ]) |> Arachnid.fromAsync
        let! id = match findExisting with
                  | Some (row) -> Arachnid.init row.Id
                  | None -> arachnid {
                    let email = Aether.Optic.get (JwtSecurityToken.claim_ "email" >?> Claim.value_) token.IdToken
                    let id = Guid.NewGuid().ToString()
                    let command = ForeScorer.Users.Domain.RegisterUser (identity, email)
                    let! _user = userCreateHandler id command |> Arachnid.fromAsync

                    return id
                  }


        let handler = JwtSecurityTokenHandler()
        let tokenDescriptor = SecurityTokenDescriptor (
                                Audience = "http://localhost:5000",
                                Issuer = "http://localhost:5000",
                                Subject = ClaimsIdentity([Claim(JwtRegisteredClaimNames.Sub, id)]),
                                SigningCredentials = ForeScorer.Global.OAuth.CodeSigningCredentials.Value,
                                Expires = Nullable(DateTime.UtcNow + TimeSpan.FromMinutes(5.0))
                              )

        let encodedToken =
          try
            handler.CreateEncodedJwt tokenDescriptor
          with
            | x ->
              printfn "Caught: %O" x
              raise x

        printfn "Encoded token: %O" encodedToken

        return Ok { Types.Code = encodedToken; Types.AuthorizationSuccess.State = state }
      | None ->
        return Error { Types.Error = Types.ErrorCode.ServerError; Types.Description = Some("Unable to retrieve user details from your identity provider"); Types.Uri = None; Types.State = state }
    | Error(err) -> return Error(err)
  }

  let representClientRedirect (resp : Types.AuthorizationResponse) = arachnid {

    let baseUri = Types.getResponseRedirectUri resp
    let redirectUri = Aether.Optic.set (Uri.query_ >?> Types.authorizationResponse_) resp baseUri

    do! Response.Headers.location_ .= (Some (Location (UriReference.parse (redirectUri.ToString()))))
    return Representation.empty
  }

  let providerRedirect config = arachnid {
    let! baseUri = Request.baseUri
    let! nonce = !. (Route.atom_ "nonce")
    let! state = !. (Route.atom_ "state")
    let! clientId = !. (Route.atom_ "client_id")
    let! redirectUri = !. (Route.atom_ "redirect_uri")

    let myNonce = generateNonce()
    let innerState = { Types.StateWrapper.InnerState = state
                     ; Types.StateWrapper.RedirectUri = redirectUri |> Option.get |> Uri.parse
                     ; Types.StateWrapper.Nonce = Some myNonce
                     }

    let req =
      { Nonce = Some myNonce
      ; RedirectUri = (Aether.Optic.set path_ ["auth"; "google"; "redirect"] baseUri)
      ; ResponseType = [Code]
      ; Scope = [OpenId; Email]
      ; State = Some(innerState |> JI.serialize |> System.Text.Encoding.UTF8.GetBytes |> Convert.ToBase64String)
      ; Prompt = None
      }
      
    let redirectUri = getAuthUrl config req
    do! Response.Headers.location_ .= (Some (Location (UriReference.parse (redirectUri.ToString()))))
    do! Response.Headers.setCookie_ .= (Some (SetCookie (Pair (Name.Name (nonceCookieName), Value myNonce), Attributes [])))
    return Representation.empty
  }

  let validateCode validationParams code =
    try
        let (_claims, token) = ForeScorer.Global.OAuth.JwtTokenHandler.Value.ValidateToken(code, validationParams)

        Ok(token :?> JwtSecurityToken)
      with
        | x ->
          Error
            { Types.AuthorizationError.Error = Types.ErrorCode.InvalidGrant
            ; Types.AuthorizationError.Description = Some(x.Message)
            ; Types.AuthorizationError.Uri = None
            ; Types.AuthorizationError.State = Types.StateWrapper.empty
          }

  let formData = Arachnid.memo (arachnid {
    let! body = !. Request.body_
    let reader = new IO.StreamReader(body)

    let! s = reader.ReadToEndAsync() |> Async.AwaitTask |> Arachnid.fromAsync

    return (fst Request.formUrlEncoded_) s
  })

  let formValue key = arachnid {
    let! data = formData

    return
      data
      |> Option.bind (List.tryFind (fst >> (=) key))
      |> Option.bind (snd >> List.tryHead)
  }


  let tokenRequest = Arachnid.memo(arachnid {
    let! grant_type = formValue "grant_type"

    match grant_type with
    | None ->
      return
        Error
          { Types.AuthorizationError.Error = Types.ErrorCode.InvalidRequest
          ; Types.AuthorizationError.Description = Some("Missing 'grant_type' parameter")
          ; Types.AuthorizationError.Uri = None
          ; Types.AuthorizationError.State = Types.StateWrapper.empty
          }
    | Some(gt) when gt <> "authorization_code" ->
      return
        Error
          { Types.AuthorizationError.Error = Types.ErrorCode.UnsupportedGrantType
          ; Types.AuthorizationError.Description = Some("'grant_type' must be 'authorization_code'")
          ; Types.AuthorizationError.Uri = None
          ; Types.AuthorizationError.State = Types.StateWrapper.empty
          }
    | _ -> 

    let! code = formValue "code"

    match code with
    | None ->
      return
        Error
          { Types.AuthorizationError.Error = Types.ErrorCode.InvalidRequest
          ; Types.AuthorizationError.Description = Some("Missing 'code' parameter")
          ; Types.AuthorizationError.Uri = None
          ; Types.AuthorizationError.State = Types.StateWrapper.empty
          }
    | Some(c) ->
      let validationParams = TokenValidationParameters(
                               ValidIssuer = "http://localhost:5000",
                               ValidAudience = "http://localhost:5000",
                               IssuerSigningKey = ForeScorer.Global.OAuth.CodeSigningCredentials.Value.Key
                             )
      return validateCode validationParams c
  })

  let createAccessToken (code : JwtSecurityToken) = arachnid {
    let! hostAndPort = Request.baseUri

    let accessClaims =
      code.Claims
      |> Seq.filter (fun (c : Claim) -> c.Type = JwtRegisteredClaimNames.Sub)

    return
      ForeScorer.Global.OAuth.JwtTokenHandler.Value.CreateJwtSecurityToken(
        SecurityTokenDescriptor(
          Audience = Uri.format hostAndPort,
          Issuer = Uri.format hostAndPort,
          Subject = ClaimsIdentity(accessClaims),
          SigningCredentials = ForeScorer.Global.OAuth.TokenSigningCredentials.Value,
          Expires = Nullable(DateTime.UtcNow + TimeSpan.FromHours(1.0))
        )
      )    
  }

  let createTokenResponse (code : JwtSecurityToken) = arachnid {
    let! accessToken = createAccessToken code

    return
      { Types.TokenSuccess.AccessToken = accessToken
      ; Types.TokenSuccess.TokenType = "Bearer"
      ; Types.TokenSuccess.ExpiresIn = TimeSpan.FromHours(1.0)
      ; Types.TokenSuccess.RefreshToken = None
      ; Types.TokenSuccess.Scope = None
      }
  }

  let tokenMachine = arachnidMachine {
    methods [ POST; OPTIONS; ]
    // TODO: Validate the client_id/client_secret!
    badRequest(
      function
      | Ok(_) -> false
      | Error(_) -> true
      <!> tokenRequest
    )
    handleBadRequest(
      function
      | Ok(_) -> Representation.empty
      | Error(e) -> Helpers.represent e
      <!> tokenRequest
    )

    handleOk(
          function
          | Ok(o) -> Helpers.represent o
          | Error(e) -> Helpers.represent e
      <!> (
                tokenRequest
            >>= function | Ok(cp) -> Ok <!> (createTokenResponse cp) | Error(e) -> e |> Error |> Arachnid.init
      )
    )
  }

  let oidcConfig = arachnid {
    let! bu = Request.baseUri
    return { Types.Configuration.Issuer = bu
           ; Types.Configuration.TokenEndpoint = Aether.Optic.set path_ ["auth"; "token"] bu 
           ; Types.Configuration.AuthorizationEndpoint = Aether.Optic.set path_ ["auth"; "google"] bu 
           ; Types.Configuration.ScopesSupported = [] }
  }

  let configMachine = arachnidMachine {
    methods [ GET; OPTIONS ]
    handleOk(Helpers.represent <!> oidcConfig)
  }

  let providerEndpoints (provider : ProviderConfig) =
    let authorizeMachine = arachnidMachine {
      methods [ GET; OPTIONS ]
      badRequest(((Option.bind (Uri.tryParse >> Result.toOption)) >> Option.isNone) <!> (!. (Route.atom_ "redirect_uri")))
      exists false
      movedTemporarily true
      handleTemporaryRedirect(providerRedirect provider)
    }

    let redirectMachine = arachnidMachine {
      methods [ GET; OPTIONS; ]
      badRequest (redirectResponse >>= badRedirect)
      exists false
      movedTemporarily true
      handleTemporaryRedirect ((Option.get <!> redirectResponse) >>= processRedirect provider >>= representClientRedirect)
    }

    authorizeMachine,redirectMachine