module ForeScorer.Authentication.Oidc.Domain

open System.Net.Http
open Aether
open Aether.Operators
open Arachnid.Types.Uri
open Arachnid.Types

open Chiron
module D  = Chiron.Serialization.Json.Decode
module JI = Chiron.Inference.Json

open System.Security.Claims
open System.IdentityModel.Tokens.Jwt

open ForeScorer.Chiron

type Scope =
  | OpenId
  | Profile
  | Email
  | Custom of string
  with
    override this.ToString() =
      match this with
      | OpenId -> "openid"
      | Profile -> "profile"
      | Email -> "email"
      | Custom c -> c

type ProviderConfig =
  { Issuer : Uri
  ; AuthorizationEndpoint : Uri
  ; TokenEndpoint : Uri
  ; DefaultScopes : Scope list
  ; ClientId : string
  ; ClientSecret : string option  
  }

type ResponseType =
  | Code
  | IdToken
  with
    override this.ToString() =
      match this with
      | Code -> "code"
      | IdToken -> "id_token"
    

type AuthenticationRequest =
  { Nonce : string option
  ; RedirectUri : Uri
  ; ResponseType : ResponseType list
  ; Scope : Scope list
  ; State : string option
  ; Prompt : string option  
  }

type TokenRequest =
  { Code : string
  ; RedirectUri : Uri
  }

let private handler = JwtSecurityTokenHandler()

type Claim with
  static member value_ : Lens<Claim, string> =
    let get = fun (c : Claim) -> c.Value
    let set = fun (v : string) (c : Claim) -> Claim(c.Type, v, c.ValueType, c.Issuer, c.OriginalIssuer, c.Subject)

    (get, set)


type TokenResponse =
  { AccessToken : string
  ; IdToken : JwtSecurityToken
  ; ExpiresIn : System.TimeSpan
  ; TokenType : string
  ; RefreshToken : string option
  }
  with
  static member FromJson (_ : TokenResponse) = jsonDecoder {
    let! access_token = D.required D.string "access_token"
    let! id_token = D.required D.string "id_token"
    let! expires_in = D.required D.int "expires_in"
    let! token_type = D.required D.string "token_type"
    let! refresh_token = D.optional D.string "refresh_token"

    return
      { AccessToken = access_token
      ; IdToken = handler.ReadJwtToken(id_token)
      ; ExpiresIn = System.TimeSpan.FromSeconds(float expires_in)
      ; TokenType = token_type
      ; RefreshToken = refresh_token
      }
  }

let googleIdentityProvider =
  { Issuer = Uri.parse "https://acounts.google.com/"
  ; AuthorizationEndpoint = Uri.parse "https://accounts.google.com/o/oauth2/v2/auth"
  ; TokenEndpoint = Uri.parse "https://www.googleapis.com/oauth2/v4/token"
  ; DefaultScopes = []
  ; ClientId = "GOOGLE_OAUTH_CLIENT_ID" |> System.Environment.GetEnvironmentVariable
  ; ClientSecret = "GOOGLE_OAUTH_CLIENT_SECRET" |> System.Environment.GetEnvironmentVariable |> Some
  }

let getAuthUrl (config : ProviderConfig) (req : AuthenticationRequest) =
  let spaceSepList =
    List.map string
      >> String.concat " "
      >> Some

  let query =
    [ ("client_id", Some config.ClientId)
    ; ("redirect_uri", req.RedirectUri |> Mapping.format Uri.Mapping |> Some)
    ; ("nonce", req.Nonce)
    ; ("scope", req.Scope |> List.map box |> spaceSepList)    
    ; ("response_type",  req.ResponseType |> List.map box |> spaceSepList)
    ; ("state", req.State)
    ; ("prompt", req.Prompt)
    ] |> List.filter (snd >> Option.isSome)

  Optic.set (Uri.query_ >?> Query.pairs_) query config.AuthorizationEndpoint

let private http = new HttpClient()

let getToken (config : ProviderConfig) (req : TokenRequest) : Async<TokenResponse option> = async {
  use content =
    new FormUrlEncodedContent(
      dict
        [ "code", req.Code
        ; "client_id", config.ClientId
        ; "client_secret", Option.defaultValue "" config.ClientSecret
        ; "redirect_uri", string req.RedirectUri
        ; "grant_type", "authorization_code"
        ]
    )

  let! resp = http.PostAsync (config.TokenEndpoint |> string |> System.Uri, content) |> Async.AwaitTask
  match resp.IsSuccessStatusCode with
  | true ->
    let! content = resp.Content.ReadAsStringAsync() |> Async.AwaitTask

    return content
           |> JI.deserialize
           |> JsonResult.toOption
  | _ ->
    return None         
}