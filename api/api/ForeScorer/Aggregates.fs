module ForeScorer.Aggregates

open Chiron
module E = Chiron.Serialization.Json.Encode
module D = Chiron.Serialization.Json.Decode

type AggregateMetadata =
  { Id       : string
  ; Revision : string
  } with
  static member Mixin (meta :AggregateMetadata, jObj : JsonObject) : JsonObject =
    jObj
    |> E.required E.string "_id" meta.Id
    |> E.required E.string "_rev" meta.Revision
  static member FromJson (_ : AggregateMetadata) = jsonDecoder {
    let! id = D.required D.string "_id"
    let! rev = D.required D.string "_rev"

    return { Id = id; Revision = rev }
  }  

type Aggregate<'TCreateCommand, 'TUpdateCommand, 'TAggregate> =
  { handleCreate : 'TCreateCommand -> 'TAggregate
  ; handleUpdate : 'TAggregate -> 'TUpdateCommand -> 'TAggregate
  }

let inline createHandler (get : string -> Async<'TResult option>, set: string -> 'TResult -> Async<unit>) (aggregate : Aggregate<'TCreateCommand, 'TUpdateCommand, 'TResult>) =
  let handle id command = async {
    let! a = get id

    let updated =
      a
      |> Option.map (fun agg -> aggregate.handleUpdate agg command)

    match updated with
    | Some res ->
      do! set id res
      return Some res
    | None ->
      return None
  }

  let handleCreate id command = async {
    let res = aggregate.handleCreate command

    printfn "About to persist: %O" res
    do! set id res

    printfn "Persisted!"
    
    return res
  }

  handle,handleCreate