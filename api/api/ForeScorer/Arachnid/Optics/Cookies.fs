module ForeScorer.Arachnid.Optics.Cookies

open Arachnid.Types.Http.State
open Arachnid.Optics.Http
open Arachnid.Core

module Request =
    open Aether.Operators
    open Arachnid.Optics
    open ForeScorer.Arachnid

    let (*private*) value_ key (tryParse, format) =
        Request.header_ key
        >-> Option.mapEpimorphism (tryParse >> Option.ofResult, format)
    module Headers =
        let cookie_ =
            value_ "Cookie" (Cookie.tryParse, Cookie.format)
    let cookie_ key =
      Headers.cookie_
      >-> Option.mapIsomorphism Cookie.pairs_
      >-> Option.bindLens (
            List.tryFind (fun (Pair (name, _value)) -> name = key),
            function
              | Some(c) -> List.map (function | Pair (name, _) when name = key -> c | p -> p)
              | None -> List.filter (fun (Pair (name, _value)) -> name = key)
          )

module Response =
    open Aether.Operators
    open Arachnid.Optics
    let (*private*) value_ key (tryParse, format) =
        Response.header_ key
        >-> Option.mapEpimorphism (tryParse >> Option.ofResult, format)
    module Headers =
        let setCookie_ =
            value_ "Set-Cookie" (SetCookie.tryParse, SetCookie.format)