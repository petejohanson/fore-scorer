module ForeScorer.Arachnid.Optics.Http

module Request =
  open System.Web
  open Arachnid.Core
  open Arachnid.Optics
  open Arachnid.Core.Operators
  open Arachnid.Optics.Http
  open Arachnid.Types.Uri

  let hostAndPort_ =
    Request.header_ "host"

  let baseUri =
       fun scheme hostAndPort ->
         Uri.Uri(scheme, HierarchyPart.Authority(Authority.parse(Option.defaultValue "localhost" hostAndPort), PathAbsoluteOrEmpty([])), None, None)
    <!> !. Request.scheme_
    <*> !. hostAndPort_

  let formUrlEncoded_ : Aether.Epimorphism<string, (string * string list) list> =
    (
      fun s ->
        try
          let c = HttpUtility.ParseQueryString(s)
          c.AllKeys
          |> Array.toList
          |> List.map (fun k -> (k, k |> c.GetValues |> Array.toList))
          |> Some
        with
        | _ -> None        
    ),
    (
      fun p ->
        p
        |> List.collect (fun (key, values) -> [for v in values -> (HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(v))])
        |> List.map (
          function
          | (key, "") -> key
          | (key, value) -> key + "=" + value
        )
        |> String.concat "&"
    )