namespace ForeScorer.Arachnid

open System.Text

open Chiron
module J = Chiron.Serialization.Json
module JI = Chiron.Inference.Json

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Optics.Http
open Arachnid.Types.Http
open Arachnid.Uber

open ForeScorer.Chiron

[<RequireQualifiedAccess>]
module Option =
  open Aether

  let bindLens (p : Lens<'a, 'b option>) : Lens<'a option, 'b option> =
    Option.bind (fst p), snd p >> Option.map

[<RequireQualifiedAccess>]
module Result =
  let toOption =
    function
    | Ok (o) -> Some o
    | _ -> None

[<RequireQualifiedAccess>]
module Helpers =
  let FORMATTING =
#if RELEASE
    JsonFormattingOptions.Compact
#else
    JsonFormattingOptions.Pretty
#endif

  let inline representWith encoder x =
    let content =
      x
      |> J.serializeWith encoder FORMATTING

    {
        Description =
          { Charset = Some Charset.Utf8
            Encodings = None
            MediaType = Some MediaType.Json
            Languages = None }
        Data = Encoding.UTF8.GetBytes content
    }    

  let inline represent x =
    let content =
      x
      |> JI.encode
      |> Json.formatWith FORMATTING

    {
        Description =
          { Charset = Some Charset.Utf8
            Encodings = None
            MediaType = Some MediaType.Json
            Languages = None }
        Data = Encoding.UTF8.GetBytes content
    }

  let representUber doc =
    let content = Arachnid.Uber.Json.serialize doc

    {
        Description =
          { Charset = Some Charset.Utf8
            Encodings = None
            MediaType = "application/vnd.uber+json" |> MediaType.parse |> Some
            Languages = None }
        Data = Encoding.UTF8.GetBytes content
    }

  let inline payload () = 
       Json.parseStream
    >> (JsonResult.bind JI.decode)
    >> JsonResult.toOption
    <!> Arachnid.Optic.get Request.body_
