module ForeScorer.Api

open Arachnid.Core
open Arachnid.Core.Operators
open Arachnid.Machines.Http
open Arachnid.Routers.Uri.Template
open Arachnid.Types.Http
open Arachnid.Types.Uri
open Arachnid.Uber

open ForeScorer.Courses.Api
open ForeScorer.Games.Api
open ForeScorer.Arachnid.Optics.Http
open ForeScorer.Authentication.BearerToken
open ForeScorer.Authentication.Oidc.Api.Resources
open ForeScorer.Authentication.Oidc.Domain
open ForeScorer.Arachnid

let (authorizeMachine, redirectMachine) = providerEndpoints googleIdentityProvider

let resolveUrls baseUrl ({ Url = url } as data) =
    let abs = (snd AbsoluteUri.uri_) baseUrl

    let newUrl = Option.map (function
                            | (Url(ref)) ->
                                Url(AbsoluteUri.Resolve abs ref |> UriReference.Uri)
                            | template -> template        
                        ) url

    { data with Url = newUrl }
    
let rootLinks = [
    { Data.empty with Url = Some(Url(Arachnid.Types.Uri.UriReference.parse "courses/")); Rel = ["http://api.forescorer.com/rels/courses/list"] }
    gameFormLink
]

let rootCollectionMachine = arachnidMachine {
    methods [GET];
    handleOk (arachnid {
        let! b = Request.baseUri
        let data = List.map (resolveUrls b) rootLinks
        
        return Helpers.representUber (Arachnid.Uber.Document (Some(Version1_0), data, None))
    });
}

let root =
    tokenParser *> (
        UriTemplateRouter.Arachnid (
            arachnidRouter {
                resource "/" rootCollectionMachine
                resource "/courses/" courseCollectionMachine
                resource "/games/" gameCollectionMachine
                resource "/games/{id}" gameMachine
                resource "/courses/{id}" courseMachine
                resource "/.well-known/openid-configuration" configMachine
                resource "/auth/token" tokenMachine
                resource "/auth/google{?nonce,redirect_uri,state,client_id,q*}" authorizeMachine
                resource "/auth/google/redirect{?q*}" redirectMachine
            }
        )
    )
